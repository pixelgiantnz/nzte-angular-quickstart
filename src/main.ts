var coreJS = require('core-js/');
var zoneJS = require('zone.js');
// var systemjs = require('systemjs/dist/system.js');

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
