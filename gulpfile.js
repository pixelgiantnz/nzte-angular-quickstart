var gulp           = require('gulp');
var runSequence    = require('run-sequence');
var notify 		     = require("gulp-notify");
var webpack        = require('webpack-stream');
var browserSync    = require('browser-sync')
var webpackConfig  = require('./webpack.config.js');

var config                  = {}

config.publicRoot  			= "./";

gulp.task('default', function ()
{
  runSequence(['angular'],['browserSync']);
});


config.angular_src           = "/src/main.ts";
config.webpack_dest          = "./src/";


gulp.task('angular', function()
{
  console.log("angular Task");

  return gulp.src(config.angular_src)
    .pipe(webpack(webpackConfig))
	  .on('error', handleErrors)
    .pipe(gulp.dest(config.webpack_dest))
    .pipe(browserSync.reload({stream: true}));

});

gulp.task('browserSync', function() {
	return browserSync(
	{
		ghostMode: {
			clicks: true,
			forms: true,
			scroll: false // Causes issues with development having scroll enabled
		},
		notify: false,
		// files  : ['public/**/*.html'], //Don't need this as we call browsersync when nunjucks has rendered
		files: false,
		server :
		{
			baseDir : [config.publicRoot]
		},
    startPath: '/src/'
	})
});

handleErrors = function(errorObject, callback)
{
  notify.onError(errorObject.toString().split(': ').join(':\n')).apply(this, arguments);
  if (typeof this.emit === 'function') this.emit('end');
};
