const path = require('path');
var webpack = require("webpack");

var bProductionMode = process.argv.indexOf("production") !== -1;

var plugins = [];
// if(bProductionMode)
// {
//     plugins.push(new webpack.optimize.DedupePlugin());
//     plugins.push(new webpack.optimize.UglifyJsPlugin({minimize: true,sourceMap:false}));
// }

    // devtool: 'source-map',

module.exports =
{
    entry: './src/main.ts',
    output:
    {
        filename: '[name].js',
        publicPath: "/src/"
    },
    plugins: plugins,
    module:
    {
        loaders:
        [
            {
                test: /\.ts$/,
                loader: 'ts-loader',
            }

        ]
    },

    resolve:
    {
      extensions: ['', '.webpack.js', '.web.js', '.ts', '.js'],
      root: path.resolve(__dirname),
    }
};

console.log("===")

if(bProductionMode)
{
    console.log("=== NOTE: Production mode used, removing source maps ===");

}
else
{
    console.log("===  NOTE: INCLUDING SOURCE MAPS, run 'gulp production' to remove them ===");
    module.exports["devtool"] = "source-map";

}

console.log("===")
